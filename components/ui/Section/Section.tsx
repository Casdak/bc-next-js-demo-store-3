import React, { Children, Component, FC, ReactNode } from 'react'
import { Container } from '@components/ui'
import s from './Section.module.css'
interface SectionProps {
  sectionTitle?: string
  sectionText?: string
  className?: string
  backgroundImage?: string
  children?: ReactNode | Component | any
}

const Section: FC<SectionProps> = ({ backgroundImage, className, sectionTitle, sectionText, children }) => {
  return (
    <div className={`bg-accent-9 border-b border-t border-accent-2 ${s.sectionWrapper}`} style={{
      backgroundImage: "url(" + `${backgroundImage}` + ")",
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
    }}
    >
      <Container className={s.container}>
        <div className={s.root}>
          <h2 className={s.title}>{sectionTitle}</h2>
          <div className={s.description}>
            <p>{sectionText}</p>
          </div>
        </div>
      </Container>
      {children}
    </div>
  )
}

export default Section
